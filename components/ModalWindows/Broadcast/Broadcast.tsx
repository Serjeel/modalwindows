import { useState } from 'react';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import { DatePicker, TimePicker } from '@mui/x-date-pickers';
import InputLabel from '@mui/material/InputLabel';
import PlusIcon from '@mui/icons-material/AddCircleOutline';
import Header from '../../Header';
import ButtonsStack from '../../ButtonsStack';
import styles from '../../../styles/Home.module.scss'
import broadcastStyle from './Broadcast.module.scss'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../redux/store';
import { setTitle } from '../../../redux/slice';
import { setDate } from '../../../redux/slice';
import { setTime } from '../../../redux/slice';

const Broadcast = () => {
    const dispatch = useDispatch();
    const title = useSelector((state: RootState) => state.title)
    const date = useSelector((state: RootState) => state.date)
    const time = useSelector((state: RootState) => state.time)

    const buttonIsDisabled: boolean = !Boolean(title && date && time);

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Button variant="outlined" onClick={handleClickOpen}>
                Open broadcast
            </Button>

            <Modal
                keepMounted
                open={open}

                aria-labelledby="keep-mounted-modal-title"
                aria-describedby="keep-mounted-modal-description"
            >
                <Stack className={styles.stack}>

                    <Header text={"Schedule broadcast"} handleClose={handleClose} />

                    <Divider />

                    <Box className={styles.box}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <Stack spacing={2} direction="column">
                                <Stack direction="column">
                                    <InputLabel shrink>
                                        Title
                                    </InputLabel>
                                    <TextField size="small" fullWidth
                                        value={title}
                                        onChange={(e) => {
                                            dispatch(setTitle(e.target.value));
                                        }} />
                                </Stack>

                                <Stack direction="column">
                                    <InputLabel shrink >
                                        Date
                                    </InputLabel>
                                    <DatePicker
                                        value={date}
                                        onChange={(newValue) => {
                                            dispatch(setDate(newValue))
                                        }}
                                        renderInput={(params) => <TextField size="small"
                                            fullWidth {...params} />}
                                    />
                                </Stack>

                                <Stack direction="column">
                                    <InputLabel shrink>
                                        Time
                                    </InputLabel>
                                    <TimePicker
                                        value={time}
                                        onChange={(newValue) => {
                                            dispatch(setTime(newValue));
                                        }}
                                        renderInput={(params) => <TextField {...params} size="small"
                                            fullWidth {...params} />}
                                    />
                                </Stack>
                            </Stack>

                            <Stack direction="column">
                                <InputLabel shrink>
                                    Thumbnail
                                </InputLabel>
                                <Button
                                    className={broadcastStyle.thumbnail}
                                    variant="contained"
                                    component="label"
                                >
                                    <PlusIcon />
                                    <input
                                        type="file"
                                        hidden
                                    />
                                </Button>
                            </Stack>

                        </LocalizationProvider>
                        <ButtonsStack text1='Add Broadcast' text2='Cancel' isDisabled={buttonIsDisabled} />
                    </Box>
                </Stack>
            </Modal >
        </>
    );
};

export default Broadcast;