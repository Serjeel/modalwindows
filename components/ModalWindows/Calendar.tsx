import { useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { StaticDatePicker } from '@mui/x-date-pickers/StaticDatePicker';

import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import Header from '../Header';
import ButtonsStack from '../ButtonsStack';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux/store';
import { setCalendarValue } from '../../redux/slice';

import styles from '../../styles/Home.module.scss'

const isWeekend = (date: Dayjs) => {
    const day = date.day();

    return day === 0 || day === 6;
};

export default function Calendar() {
    const dispatch = useDispatch();
    const calendarValue = useSelector((state: RootState) => state.calendarValue)

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Button variant="outlined" onClick={handleClickOpen}>
                Open calendar
            </Button>

            <Modal
                keepMounted
                open={open}

                aria-labelledby="keep-mounted-modal-title"
                aria-describedby="keep-mounted-modal-description"
            >
                <Stack className={styles.stack}>

                    <Header text='Pick a date' handleClose={handleClose} />

                    <Divider />

                    <Box className={styles.box}>

                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <StaticDatePicker
                                displayStaticWrapperAs="desktop"
                                label="Week picker"
                                openTo="day"
                                value={calendarValue}
                                shouldDisableDate={isWeekend}
                                onChange={(newValue) => {
                                    dispatch(setCalendarValue(newValue));
                                }}
                                renderInput={(params) => <TextField {...params} />}
                            />
                        </LocalizationProvider>
                        <ButtonsStack text1='Save' text2='Cancel' isDisabled={false} />
                    </Box>
                </Stack>
            </Modal>
        </>
    );
}
