import { NextPage } from 'next';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import styles from '../styles/Home.module.scss';

interface Props {
    text1: string,
    text2: string,
    isDisabled: boolean
}

const ButtonsStack: NextPage<Props> = ({ text1, text2, isDisabled }) => {

    return (
        <Stack spacing={2} direction="column">
            <Button className={styles.button}
                variant="contained" disabled={isDisabled}>{text1}</Button>
            <Button className={styles.button}
                color="error" variant="outlined">{text2}</Button>
        </Stack>
    );
}

export default ButtonsStack;