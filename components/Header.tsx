import { NextPage } from 'next';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';

import styles from '../styles/Home.module.scss'

interface Props {
    text: string,
    handleClose: () => void
}

const Header: NextPage<Props> = ({ text, handleClose }) => {

    return (
        <Typography gutterBottom className={styles.header} id="keep-mounted-modal-title"
            variant="h5" component="h2">
            {text}
            <CloseIcon sx={{ cursor: 'pointer' }} onClick={handleClose} />
        </Typography>
    );
};

export default Header;