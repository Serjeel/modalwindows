import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import dayjs, { Dayjs } from 'dayjs';

type StateType = {
    title: string,
    date: Dayjs | null,
    time: Dayjs | null,
    calendarValue: Dayjs | null
}

const initialState: StateType = {
    title: "",
    date: dayjs(),
    time: dayjs(),
    calendarValue: dayjs()
}

const toolkitSlice = createSlice({
    name: "toolkit",
    initialState,
    reducers: {
        setTitle(state, action: PayloadAction<string>) {
            state.title = action.payload;
        },
        setDate(state, action: PayloadAction<Dayjs | null>) {
            state.date = action.payload;
        },
        setTime(state, action: PayloadAction<Dayjs | null>) {
            state.time = action.payload;
        },
        setCalendarValue(state, action: PayloadAction<Dayjs | null>) {
            state.calendarValue = action.payload;
        }
    }
})

export const { setTitle } = toolkitSlice.actions;
export const { setDate } = toolkitSlice.actions;
export const { setTime } = toolkitSlice.actions;
export const { setCalendarValue } = toolkitSlice.actions;

export default toolkitSlice.reducer;